<?php


namespace App\ViewModels;


class ConvertCurrencyViewModel extends BaseViewModel
{
    /** @var float */
    public $amount;

    /** @var string */
    public $currency;
}
