<?php


namespace App\Enums;


class CurrencyEnum extends BaseEnum
{
    public const PLN = 'PLN';
    public const USD = 'USD';
    public const EUR = 'EUR';
    public const GBP = 'GBP';
    public const JPY = 'JPY';
    public const AUD = 'AUD';
    public const CAD = 'CAD';
    public const CHF = 'CHF';
}
