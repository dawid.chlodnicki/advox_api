<?php


namespace App\Enums;


use ReflectionClass;

abstract class BaseEnum
{
    /**
     * @return array
     */
    public static function getConstants(): array
    {
        $class = new ReflectionClass(static::class);

        return $class->getConstants();
    }
}
