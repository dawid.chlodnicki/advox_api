<?php


namespace App\Services;


use App\Enums\CurrencyEnum;
use App\Http\Requests\CurrencyConvertRequest;
use App\ViewModels\ConvertCurrencyViewModel;
use Illuminate\Support\Facades\Http;

class CurrencyService extends BaseService
{
    /**
     * @return array
     */
    public function index(): array
    {
        return CurrencyEnum::getConstants();
    }

    /**
     * @param  CurrencyConvertRequest  $request
     * @return ConvertCurrencyViewModel|null
     */
    public function convert(CurrencyConvertRequest $request): ?ConvertCurrencyViewModel
    {
        $endpoint = config('endpoints.exchange-rates-api');
        $response = Http::get($endpoint, ['base' => $request->input('from'), 'symbols' => $request->input('to')]);

        if ($response->status() === 200) {
            $json = $response->object();
            $exchangeRate = $json->rates->{$request->input('to')};

            $viewModel = new ConvertCurrencyViewModel();
            $viewModel->amount = $request->input('amount') * $exchangeRate;
            $viewModel->currency = $request->input('to');

            return $viewModel;
        }

        return null;
    }
}
