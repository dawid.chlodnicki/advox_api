<?php


namespace App\Http\Controllers;


use App\Http\Requests\CurrencyConvertRequest;
use App\Services\CurrencyService;
use Illuminate\Http\JsonResponse;

class CurrencyController extends Controller
{
    /**
     * CurrencyController constructor.
     * @param  CurrencyService  $service
     */
    public function __construct(CurrencyService $service)
    {
        $this->service = $service;
    }

    /**
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        return $this->getSuccessResponse($this->service->index());
    }

    /**
     * @param  CurrencyConvertRequest  $request
     * @return JsonResponse
     */
    public function convert(CurrencyConvertRequest $request): JsonResponse
    {
        $viewModel = $this->service->convert($request);

        if ($viewModel !== null) {
            return $this->getSuccessResponse($viewModel);
        }

        return $this->getBadRequestResponse();
    }
}
