<?php

namespace App\Http\Controllers;

use App\Services\BaseService;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\JsonResponse;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /** @var BaseService */
    protected $service;

    /**
     * @param  null  $data
     * @return JsonResponse
     */
    protected function getSuccessResponse($data = null): JsonResponse
    {
        return response()->json($data, 200);
    }

    /**
     * @param  null  $data
     * @return JsonResponse
     */
    protected function getBadRequestResponse($data = null): JsonResponse
    {
        return response()->json($data, 400);
    }
}
