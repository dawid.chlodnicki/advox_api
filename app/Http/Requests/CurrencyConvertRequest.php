<?php

namespace App\Http\Requests;

use App\Enums\CurrencyEnum;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CurrencyConvertRequest extends FormRequest
{
    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'amount' => ['required', 'numeric'],
            'from' => ['required', Rule::in(CurrencyEnum::getConstants())],
            'to' => ['required', Rule::in(CurrencyEnum::getConstants())]
        ];
    }
}
