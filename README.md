## Installation
1. Copy _.env_ from _.env.example_ and configure application.
2. Run commands:
    1. `composer install`
    2. `php artisan key:generate`
    3. `php artisan optimze:clear`
    
If you want to run the application locally, use the command: `php artisan serve`
